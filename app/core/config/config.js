(function () {
    'use strict';

    angular.module('app').config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider', 'paginationTemplateProvider', 'Constant'];

    function config($stateProvider, $urlRouterProvider, paginationTemplateProvider, Constant) {

        $urlRouterProvider.otherwise('/home');

        $stateProvider
            .state('home', {
                url: '/home',
                templateUrl: 'main/home/view/home.html',
                controller: 'homeController as vm',
                resolve: {
                    simpleObj: function () {
                        return {value: 'simple!'};
                    }
                }
            })
            .state('signup', {
                url: '/signup',
                templateUrl: 'layout/header/signup.html',
                controller: 'signupController as vm'
            })
            .state('signin', {
                url: '/signin',
                templateUrl: 'layout/header/signin.html',
                controller: 'signinController as vm'
            })
            .state('popup', {
                url: '/popup',
                templateUrl: 'popup/popup.html',
                controller: 'popupController as vm'
            })
            .state('product', {
                url: '/product',
                template: '<ui-view></ui-view>'
            })
            .state('product.cat', {
                url: '/:categoryId',
                templateUrl: 'main/product/list/product-list.html',
                controller: 'productListController as vm',
                resolve: {
                    ProductList: function (Constant, Common, $stateParams) {
                        var url = Constant.URL + Constant.CLIENT.SERVICES.GET_All_PRODUCT_BY_CATEGORY;
                        var params = {
                            category: $stateParams.categoryId
                        };

                        return Common.serviceCall('GET', url, params)
                            .then(function (response) {
                                return response.data;
                            });
                    }
                }
            })
            .state('product.cat.sub', {
                url: '/:subCategoryId',
                templateUrl: 'main/product/list/product-list.html',
                controller: 'productListController as vm',
                resolve: {
                    ProductList: function (Constant, Common, $stateParams) {
                        var url = Constant.URL + Constant.CLIENT.SERVICES.GET_All_PRODUCT_BY_CATEGORY;
                        var params = {
                            category: $stateParams.categoryId,
                            subcategory: $stateParams.subCategoryId
                        };

                        return Common.serviceCall('GET', url, params)
                            .then(function (response) {
                                return response.data;
                            });
                    }
                }
            })
           /* .state('product.grid', {
                url: '/grid',
                templateUrl: 'main/product/grid/product-grid.html',
                controller: 'productGridController as vm'
            })*/
            .state('product.cat.sub.detail', {
                url: '/:id',
                templateUrl: 'main/product/details/product-details.html',
                controller: 'productDetailsController as vm',
                resolve: {
                    ProductList: function (Constant, Common, $stateParams) {
                        var url = Constant.URL + Constant.CLIENT.SERVICES.GET_PRODUCT_BY_ID;
                        var params = {
                            yoId: $stateParams.id
                        };

                        return Common.serviceCall('GET', url, params)
                            .then(function (response) {
                                return response.data;
                            });
                    }
                }
            });

        //$locationProvider.html5Mode(true); //For Remove #


        paginationTemplateProvider.setPath(Constant.PAGINATION_TEMP);
    }

})();
