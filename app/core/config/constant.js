(function () {
    'use strict';

    angular.module('app').constant('Constant', {
        URL:'http://10.0.0.19:8080/YoPillz',
        LOADER:true,
        IMAGE_PATH: 'assets/img/',
        JSON: 'assets/json/',
        PAGINATION_TEMP: 'layout/pagination/dirPagination.tpl.html',
        LOGIN:{
            VALIDATIONS:{},
            SERVICES:{
                AUTH:'/authentication',
                VALID_AUTH:'/validateauthtoken',
                GET_USER_DETAIL:'/userdetail',
                LOGOUT:'/logout'
            }
        },
        CLIENT:{
            VALIDATIONS:{},
            SERVICES:{
                GET_All_PRODUCT_BY_CATEGORY:'/getallproductbycategory',
                GET_PRODUCT_BY_ID:'/getproductbyid'
            }
        }
    });

})();
