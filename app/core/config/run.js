(function () {
    'use strict';

    angular.module('app').run(run);

    run.$inject = ['$rootScope', '$state', 'Authentication'];

    function run($rootScope, $state, Authentication) {

        $rootScope.$on('$stateChangeStart',
            function(event, toState, toParams, fromState, fromParams, options){
                var stateName = toState.name || '';
                var authTokan = Authentication.getAuthTokan();

                if (stateName === 'home'){
                    if (authTokan !== ''){
                        event.preventDefault();
                        $state.go('dashboard');
                    }
                }else{
                    if (authTokan === ''){
                        event.preventDefault();
                        $state.go('login');
                    }
                }

                if (toState.redirectTo) {
                    event.preventDefault();
                    $state.go(toState.redirectTo, toParams, {location: 'replace'});
                }
            });

        $rootScope.$on('$stateChangeSuccess',
            function (event, toState, toParams, fromState, fromParams) {
            });

    }

})();
