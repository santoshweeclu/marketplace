(function () {
    'use strict';
    angular.module('app').directive('stickHeader', function () {
        var directive = {
            link: linkFunction
        };

        return directive;
    });

    function linkFunction(scope, elem, attr) {
        var offset = $(attr.stickPoint).offset().top;
        //.header-mid-area .logo-area img
        $(window).scroll(function (e) {
            if ($(window).width() > 768) {
                $(elem).find('.header-top').addClass('header-top-stick');
                $(elem).find('.header-mid-area').addClass('header-mid-area-stick');
                $(elem).find('.header-bottom-area').addClass('header-bottom-area-stick');
                if ($(window).scrollTop() > offset) {
                    $(elem).find('.header-mid-area').addClass('after-scroll-point');
                } else {
                    $(elem).find('.header-mid-area').removeClass('after-scroll-point');
                }
            } else {
                $(elem).find('.header-top').removeClass('header-top-stick');
                $(elem).find('.header-mid-area').removeClass('header-mid-area-stick ');
                $(elem).find('.header-bottom-area').removeClass('header-bottom-area-stick');
            }

        });
    }
})();


