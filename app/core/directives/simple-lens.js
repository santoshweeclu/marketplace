(function () {
    'use strict';
    angular.module('app').directive('simpleLens', function () {
        var directive = {
            link: linkFunction
        };

        return directive;
    });

    function linkFunction(scope, elem, attr) {
        $(elem).simpleLens({
            loading_image: 'assets/img/loading.gif'
        });
    }
})();

