(function () {
    'use strict';
    angular.module('app').directive('pieChart', function () {
        var loader = {
            template: '<div class="pie-chart" data-percent="{{percent}}"><div ng-transclude></div></div>',
            replace: true,
            transclude: true,
            link: linkFunction,
            scope:{
                percent:'@'
            }
        };

        return loader;
    });

    function linkFunction(scope, elem, attr) {
        var easyPieChartDefaults = {
            animate: 2000,
            scaleColor: false,
            lineWidth: 6,
            lineCap: 'square',
            size: 90,
            trackColor: '#e5e5e5'
        };
        $(elem).easyPieChart($.extend({}, easyPieChartDefaults, {
            barColor: PixelAdmin.settings.consts.COLORS[1]
        }));
    }
})();

