(function () {
    'use strict';
    angular.module('app').directive('meanMenu', function () {
        var directive = {
            link: linkFunction
        };

        return directive;
    });

    function linkFunction(scope, elem, attr) {
        $(elem).meanmenu({
            meanScreenWidth: "991",
            meanMenuContainer: attr.menuClass,
        });
    }
})();

