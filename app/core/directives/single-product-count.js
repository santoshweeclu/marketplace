(function () {
    'use strict';
    angular.module('app').directive('productCount', function () {
        var directive = {
            link: linkFunction
        };

        return directive;
    });

    function linkFunction(scope, elem, attr) {
        $(elem).prepend('<div class="dec qtybutton">-</div>');
        $(elem).append('<div class="inc qtybutton">+</div>');
        $(elem).find('.qtybutton').on('click', function() {
            var $button = $(this);
            var newVal = 0;
            var oldValue = $button.parent().find('input').val();
            if ($button.text() == '+') {
                newVal = parseFloat(oldValue) + 1;
            } else {
                // Don't allow decrementing below zero
                if (oldValue > 0) {
                    newVal = parseFloat(oldValue) - 1;
                } else {
                    newVal = 0;
                }
            }
            $button.parent().find('input').val(newVal);
        });
    }
})();

