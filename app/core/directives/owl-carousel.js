(function () {
    'use strict';
    angular.module('app').directive('owlCarousel', function () {
        var directive = {
            link: linkFunction
        };

        return directive;
    });

    function linkFunction(scope, elem, attr) {
        $(elem).owlCarousel({
            autoPlay: false,
            slideSpeed:2000,
            pagination:false,
            navigation:true,
            items : attr.items || 6,
            /* transitionStyle : "fade", */    /* [This code for animation ] */
            navigationText:["<i class='fa fa-angle-right'></i>","<i class='fa fa-angle-left'></i>"],
            itemsDesktop : [1199,3],
            itemsDesktopSmall : [980,3],
            itemsTablet: [768,2],
            itemsMobile : [479,1],
        });
    }
})();

