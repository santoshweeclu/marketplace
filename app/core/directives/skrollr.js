(function () {
    'use strict';
    angular.module('app').directive('skrollr', function () {
        var directive = {
            link: linkFunction
        };

        return directive;
    });

    function linkFunction(scope, elem, attr) {
        skrollr.init({forceHeight: false}).refresh();
    }
})();
