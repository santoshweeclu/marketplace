(function () {
    'use strict';
    angular.module('app').directive('sparkLineChart', function () {
        var loader = {
            template: '<div class="stats-sparklines" style="width: 100%"></div>',
            replace: true,
            link: linkFunction
        };

        return loader;
    });

    function linkFunction(scope, elem, attr) {
        $(elem).pixelSparkline([275,490,397,487,339,403,402,312,300], {
            type: 'line',
            width: '100%',
            height: '45px',
            fillColor: '',
            lineColor: '#fff',
            lineWidth: 2,
            spotColor: '#ffffff',
            minSpotColor: '#ffffff',
            maxSpotColor: '#ffffff',
            highlightSpotColor: '#ffffff',
            highlightLineColor: '#ffffff',
            spotRadius: 4
        });
    }
})();

