(function () {
    'use strict';
    angular.module('app').directive('lineChart', function () {
        var loader = {
            template: '<div class="graph" style="height: 230px;"></div>',
            replace: true,
            link: linkFunction
        };

        return loader;
    });

    function linkFunction(scope, elem, attr) {
        new Morris.Line({
            element: elem,
            data: [
                {day: '2014-03-10', v: 20},
                {day: '2014-03-11', v: 10},
                {day: '2014-03-12', v: 15},
                {day: '2014-03-13', v: 12},
                {day: '2014-03-14', v: 5},
                {day: '2014-03-15', v: 5},
                {day: '2014-03-16', v: 20}
            ],
            xkey: 'day',
            ykeys: ['v'],
            labels: ['Value'],
            lineColors: ['#fff'],
            lineWidth: 2,
            pointSize: 4,
            gridLineColor: 'rgba(255,255,255,.5)',
            resize: true,
            gridTextColor: '#fff',
            xLabels: 'day',
            xLabelFormat: function (d) {
                return ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'][d.getMonth()] + ' ' + d.getDate();
            },
        });
    }
})();

