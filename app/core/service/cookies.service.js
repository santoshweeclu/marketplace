(function () {
    'use strict';
    angular.module('app').factory('Authentication', authentication);

    authentication.$inject = ['$http', '$cookies'];

    function authentication($http, $cookies) {
        return {
            getAuthTokan: getAuthTokan,
            removeAuthTokan: removeAuthTokan,
            addAuthTokan: addAuthTokan
        };

        function getAuthTokan() {
            return $cookies.get('authentication') || '';
        }

        function removeAuthTokan() {
            $cookies.remove('authentication');
        }

        function addAuthTokan(data) {
            $cookies.put('authentication', data);
        }

        function verifyAuthTokan(verificationToken) {

            return $http.get(Constant.URL + Constant.LOGIN.SERVICES.GET_USER_DETAIL, {
                    header: {
                        authId: verificationToken
                    }
                })
                .then(success)
                .catch(error);

            function success(response) {
                return response.data;
            }

            function error(error) {
                console.log(error.data);
            }
        }
    }
})();
