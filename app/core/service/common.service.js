(function () {
    'use strict';
    angular.module('app').factory('Common', common);

    common.$inject = ['$http'];

    function common($http) {
        return {
            data: {},
            serviceCall: serviceCall
        };

        function serviceCall(method, url, params, headers, data) {
            return $http({
                method: method,
                url: url,
                params: params || '',
                headers: headers || '',
                data: data || ''
            }).then(successFunction).catch(errorFunction);

            // SUCCESS
            function successFunction(response) {
                return response.data;
            }

            // ERROR
            function errorFunction(response) {
                // Attach the data
                return response;
            }
        }
    }
})();
