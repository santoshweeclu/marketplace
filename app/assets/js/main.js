(function ($) {
 "use strict";



    /*----------------------------
    jQuery MeanMenu
    ------------------------------ */
        /*$('nav#dropdown').meanmenu({
        meanScreenWidth: "991",
        meanMenuContainer: ".mobile-menu",
    });*/


 /*--------------------------
 scrollUp
---------------------------- */
	/*$.scrollUp({
        scrollText: '<i class="fa fa-angle-up"></i>',
        easingType: 'linear',
        scrollSpeed: 900,
        animation: 'fade'
    }); */

/*----------------------------
 wow js active
------------------------------ */
 new WOW().init();

 /* about us page counter */
	$('.about-counter').counterUp({
		delay: 10,
		time: 1000
	});


 /* 404 us page counter */
	$('.counter').counterUp({
		delay: 5,
		time: 250
	});


/*---------------------
 price slider
--------------------- */
	$(function() {
	  $( "#slider-range" ).slider({
	   range: true,
	   min: 40,
	   max: 600,
	   values: [ 60, 570 ],
	   slide: function( event, ui ) {
		$( "#amount" ).val( "Â£" + ui.values[ 0 ] + " - Â£" + ui.values[ 1 ] );
	   }
	  });
	  $( "#amount" ).val( "Â£" + $( "#slider-range" ).slider( "values", 0 ) +
	   " - Â£" + $( "#slider-range" ).slider( "values", 1 ) );
	});


/*--
    Simple Lens and Lightbox
    ------------------------*/
	$('.simpleLens-lens-image').simpleLens({
		loading_image: 'img/loading.gif'
	});

 /*-------------------------
    single-product count
    --------------------------*/

    $(".cart-plus-minus").prepend('<div class="dec qtybutton">-</div>');
     $(".cart-plus-minus").append('<div class="inc qtybutton">+</div>');
     $(".qtybutton").on("click", function() {
      var $button = $(this);
      var oldValue = $button.parent().find("input").val();
      if ($button.text() == "+") {
        var newVal = parseFloat(oldValue) + 1;
      } else {
         // Don't allow decrementing below zero
        if (oldValue > 0) {
       var newVal = parseFloat(oldValue) - 1;
       } else {
       newVal = 0;
        }
        }
      $button.parent().find("input").val(newVal);
     });

})(jQuery);
