(function () {
    'use strict';
    angular.module('app').component('appSidebar', {
        templateUrl: 'layout/sidebar/sidebar.html',
        controller: sidebarController,
        controllerAs: 'vm',
        bindings: {
            state: '='
        }
    });

    sidebarController.$inject = ['$state'];

    function sidebarController($state) {
        /*jshint validthis: true */
        var vm = this;
    }
})();
