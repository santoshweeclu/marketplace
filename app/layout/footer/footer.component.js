(function () {
    'use strict';
    angular.module('app').component('appFooter', {
        templateUrl: 'layout/footer/footer.html',
        controllerAs: 'vm',
        controller: footerController
    });

    footerController.$inject = ['Constant'];

    function footerController(Constant) {
        /*jshint validthis: true */
        var vm = this;
        vm.imagePath = Constant.IMAGE_PATH;
    }
})();
