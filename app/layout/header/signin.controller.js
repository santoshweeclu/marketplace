(function () {
    'use strict';
    angular.module('app').controller('signinController', signinController);

    signinController.$inject = ['$scope', '$state', 'Common', 'Authentication', 'toaster', 'Constant'];

    function signinController($scope, $state, Common, Authentication, toaster, Constant) {
        /*jshint validthis: true */

        var vm = this;

        vm.login = {};
        vm.state = $state;

        vm.loginFunction = loginFunction;

        //Authentication.removeAuthTokan();

        function loginFunction() {

            var url = Constant.URL + Constant.LOGIN.SERVICES.AUTH;

            Common.serviceCall('POST', url, '', '', vm.login)
                .then(function (response) {
                    if (response.code === 0) {
                        Authentication.addAuthTokan(response.data.sToken);
                        $state.go('dashboard');
                    } else {
                        toaster.pop('danger', 'Alert', response.message);
                    }

                });
        }
    }

})();
