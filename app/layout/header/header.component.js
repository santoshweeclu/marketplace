(function () {
    'use strict';
    angular.module('app').component('appHeader', {
        templateUrl: 'layout/header/header.html',
        controllerAs: 'vm',
        controller: headerController,
        bindings: {
            state: '='
        }
    });

    headerController.$inject = ['$state', 'Authentication', 'toaster', 'Common', 'Constant'];

    function headerController($state, Authentication, toaster, Common, Constant) {
        /*jshint validthis: true */
        var vm = this;
        vm.imagePath = Constant.IMAGE_PATH;

        vm.logoutFunction = logoutFunction;

        vm.userInfo = {};
        vm.userInfo.firstName = 'Admin';

        vm.productCategory = [];

        var AuthTokan = Authentication.getAuthTokan();
        var url = '';
        var header = {
            authId: ''
        };

        getProductCategories();

        if (AuthTokan !== '') {
            init();
        }

        function init() {
            url = Constant.URL + Constant.LOGIN.SERVICES.GET_USER_DETAIL;
            header.authId = AuthTokan;

            getUserDetail(url, header);
            autoCompleteFunction();

        }

        function getUserDetail(url, header) {
            Common.serviceCall('GET', url, '', header, '')
                .then(function (response) {
                    switch (response.code) {
                        case 0:
                            vm.userInfo = response.data;
                            break;
                        default:
                            Authentication.removeAuthTokan();
                            $state.go('login');
                            console.log(response.message);
                    }
                })
                .catch(function (error) {
                    console.log('Get User Detail Service Error', error);
                });
        }

        function getProductCategories() {
            Common.serviceCall('GET', Constant.JSON + 'products-category.json')
                .then(function (response) {
                    vm.productCategory = response;
                });
        }

        function autoCompleteFunction() {
            Common.serviceCall('GET', 'http://localhost:9001/assets/json/counties.json')
                .then(function (response) {
                    vm.countries = response;
                })
                .catch(function (error) {
                    console.log('autoComplete Service Error', error);
                });
        }

        function logoutFunction() {
            url = Constant.URL + Constant.LOGIN.SERVICES.LOGOUT;
            header.authId = AuthTokan;

            Common.serviceCall('POST', url, '', header, '')
                .then(function (response) {
                    Authentication.removeAuthTokan();
                    toaster.pop('success', 'Good Bye...', vm.userInfo.firstName);
                    $state.go('login');
                })
                .catch(function (error) {
                    console.log('Logout Service Error', error);
                });
        }
    }
})();
