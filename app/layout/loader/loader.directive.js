(function () {
    'use strict';
    angular.module('app').directive('appLoader', function (Constant) {
        var directive = {
            templateUrl: 'layout/loader/loader.html',
            link: function (scope, elem, attr) {
                scope.widthStyle = attr.widthStyle;
                scope.show = Constant.LOADER;
            }
        };
        return directive;
    });
})();

