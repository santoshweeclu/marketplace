(function () {
    'use strict';

    angular.module('app', [
        'ngCookies',
        'ngMessages',
        'ngTouch',
        'ui.bootstrap',
        'ui.router',
        'ngMap',
        'ngTable',
        'toaster',
        'angular-loading-bar',
        'ngAnimate',
        'angularUtils.directives.dirPagination',
        'xeditable',
        'ngFileUpload',
        'angucomplete-alt'
    ]);

})();
