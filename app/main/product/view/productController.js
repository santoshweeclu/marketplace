(function () {
    'use strict';
    angular.module('app').controller('productController', productController);

    productController.$inject = ['$state', 'toaster', 'Constant', 'Common','$http'];

    function productController($state, toaster, Constant, Common, $http) {
        /*jshint validthis: true */
        var vm = this;
        vm.state = $state;

        vm.productDetailFunction = productDetailFunction;

        init();

        function init() {
            $state.go('product.list');
            /*Common.serviceCall('GET',Constant.JSON_PATH + 'product-list.json')
                .then(successFunction)
                .catch(errorFunction);

            function successFunction(data) {
                vm.productList = data;
            }

            function errorFunction() {
                console.log('Error with JSON file');
            }*/
        }

        function clickFunction() {
            toaster.pop('success', 'title', 'text');
        }

        function productDetailFunction() {
        }
    }
})();
