    (function () {
    'use strict';
    angular.module('app').controller('productDetailsController', productDetailsController);

    productDetailsController.$inject = ['$state', 'toaster', 'Constant', 'Common'];

    function productDetailsController($state, toaster, Constant, Common) {
        /*jshint validthis: true */
        var vm = this;
        vm.productDetail = false;
        vm.productSelected = {};
        vm.productDetailFunction = productDetailFunction;
        init();
        function init() {
            Common.serviceCall('GET', Constant.JSON_PATH + 'product-detail.json')
                .then(function (data) {
                    vm.productSelected = data;
                });
        }
        function productDetailFunction(){

        }
    }
})();
