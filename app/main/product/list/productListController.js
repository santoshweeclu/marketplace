(function () {
    'use strict';
    angular.module('app').controller('productListController', productListController);

    productListController.$inject = ['$state', 'toaster', 'Constant', 'Common','ProductList'];

    function productListController($state, toaster, Constant, Common, ProductList) {
        /*jshint validthis: true */
        var vm = this;
        vm.state = $state;
        vm.productList = ProductList || [];
        vm.currentPage = 1;
        vm.pageLower = 1;
        vm.pageUpper = 10;
        vm.productDetailFunction = productDetailFunction;
        vm.pageChangeHandler = pageChangeHandler;

        init();

        function init() {
            console.log(ProductList);
            toaster.pop('success', 'title', 'text');
        }

        function clickFunction() {
            toaster.pop('success', 'title', 'text');
        }

        function productDetailFunction() {
        }

        function pageChangeHandler(newPage, oldPage) {
            console.log('going to page ' , newPage, oldPage);
        }

        //$('#myModal').modal('show');
    }
})();
