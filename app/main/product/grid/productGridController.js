(function () {
    'use strict';
    angular.module('app').controller('productGridController', productGridController);

    productGridController.$inject = ['$state', 'toaster', 'Constant', 'Common','ProductList'];

    function productGridController($state, toaster, Constant, Common, ProductList) {
        /*jshint validthis: true */
        var vm = this;
        vm.state = $state;
        vm.productList = ProductList;
        vm.currentPage = 1;
        vm.pageLower = 1;
        vm.pageUpper = 10;
        vm.productDetailFunction = productDetailFunction;
        vm.pageChangeHandler = pageChangeHandler;

        init();

        function init() {

        }

        function clickFunction() {
            toaster.pop('success', 'title', 'text');
        }

        function productDetailFunction() {
        }

        function pageChangeHandler(newPage, oldPage) {
            console.log('going to page ' , newPage, oldPage);
        }
    }
})();
