(function () {
    'use strict';
  angular.module('app').controller('homeController', homeController);

    homeController.$inject = ['$scope','$state', 'simpleObj', 'toaster', 'Constant', '$timeout' ];

    function homeController( $scope,$state, simpleObj, toaster, Constant, $timeout) {
        /*jshint validthis: true */

        var vm = this;
        vm.state = $state;
        vm.imagePath = Constant.IMAGE_PATH;
        vm.heading = simpleObj.value;
        vm.click = clickFunction;
        //$scope.places= 'null';
        function clickFunction() {
            toaster.pop('success', 'title', 'text');
        }


        $scope.selected = undefined;
        //lookup values
        $scope.countries = [
            {name: 'Madhapur'},
            {name: 'Gachibowli' },
            {name: 'kukatpally'},
            {name: 'miyapur'},
            {name: 'panjagutta'},
            {name: 'kondapur'},
            {name: 'begumpet'},
            {name: 'Malkajgiri'},
            {name: 'Mehadipatnam'},
            {name: 'Hi-tech city'},
        ];

/*---local storage the data----*/

       /*$scope.localStorageDemo = localStorageService.get('localStorageDemo');

        $scope.$watch('localStorageDemo', function(value){
            localStorageService.set('localStorageDemo',value);
            $scope.localStorageDemoValue = localStorageService.get('localStorageDemo');
        });

        $scope.storageType = 'Local storage';

console.log($scope.storageType,'local storage data');
       $scope.$watch(function(){
            return localStorageService.get('localStorageDemo');
        }, function(value){
            $scope.localStorageDemo = value;
        });

        $scope.clearAll = localStorageService.clearAll;*/
    }
})();
