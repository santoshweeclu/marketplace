var distDir = 'dist/';
var appDir = 'app/';
var buildDir = 'app/build/';
var assetsDir = 'app/assets/';
var mainDir = 'app/main/';
var coreDir = 'app/core/';
var layoutDir = 'app/layout/';
var sourceDir = 'source/';
var vendorDir = 'vendor/';
var vendorMainDir = 'vendor/main/';
var vendorAngularDir = 'vendor/angular/';
var vendorNormalDir = 'vendor/normal/';
var vendorSayiaDir = 'vendor/sayia/';
var appJsFiles = appDir + '**/*.js';
module.exports = function (grunt) {
    // CONFIGURE GRUNT ===========================================================
    grunt.initConfig({

        // get the configuration info from package.json ----------------------------
        // this way we can use things like name and version (pkg.name)
        pkg: grunt.file.readJSON('package.json'),
        appsourceName: 'app',

        // configure jshint to validate js files -----------------------------------
        jshint: {
            all: ['Gruntfile.js', coreDir + '**/*.js', mainDir + '**/*.js', layoutDir + '**/*.js']
        },

        concat: {
            options: {
                separator: '\n',
            },
            buildVendor: {
                src: [
                    vendorMainDir + '*.js',
                    vendorAngularDir + '*.js',
                    vendorNormalDir + '*.js',
                    vendorSayiaDir + '*.js'
                ],
                dest: sourceDir + 'vendor.js'
            },
            build: {
                src: [appDir + '**/*.module.js', coreDir + '**/*.js', mainDir + '**/*.js', layoutDir + '**/*.js'],
                dest: sourceDir + '<%=appsourceName%>.js'
            }
        },

        // configure cssmin to minify css files ------------------------------------
        cssmin: {
            options: {
                banner: '/*\n <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> \n*/\n'
            },
            buildVendor: {
                files: {
                    'app/build/vendor.min.css': [
                        vendorMainDir + '*.css',
                        vendorMainDir + '*.min.css',
                        vendorAngularDir + '*.css',
                        vendorAngularDir + '*.min.css',
                        vendorNormalDir + '*.css',
                        vendorNormalDir + '*.min.css',
                        vendorSayiaDir + '*.css',
                        vendorSayiaDir + '*.min.css'
                    ]
                }
            },
            build: {
                files: {
                    'app/build/app.min.css': [assetsDir + 'css/*.css']
                }
            }
        },

        // cleaning directory -------------------------------------

        clean: {
            buildVendor: [buildDir, sourceDir],
            distCss: [distDir + 'assets/css/*'],
            distJs: [distDir + 'layout/**/*.js', distDir + 'main/**/*.js']
        },

        // configure uglify to minify js files -------------------------------------
        uglify: {
            options: {
                banner: '/*\n <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> \n*/\n',
                mangle: true
            },
            buildVendor: {
                src: sourceDir + 'vendor.js',
                dest: buildDir + 'vendor.min.js'
            },
            build: {
                src: sourceDir + 'app.js',
                dest: buildDir + 'app.min.js'
            }
        },

        // copy js files -----------------------------------
        copy: {
            build: {
                files: [
                    {
                        expand: true,
                        cwd: sourceDir,
                        src: 'app.js',
                        dest: appDir + 'build/',
                        filter: 'isFile'
                    },
                    {
                        expand: true,
                        cwd: sourceDir,
                        src: 'vendor.js',
                        dest: appDir + 'build/',
                        filter: 'isFile'
                    }
                ],
            },
            folder: {
                files: [
                    {
                        expand: true,
                        cwd: 'app',
                        src: ['main/**', 'assets/**', 'layout/**'],
                        dest: distDir
                    }
                ],
            },
            buildFiles: {
                files: [
                    {
                        expand: true,
                        cwd: buildDir,
                        src: '*.css',
                        dest: distDir + 'assets/css/',
                        filter: 'isFile'
                    },
                    {
                        expand: true,
                        cwd: buildDir,
                        src: '*.js',
                        dest: distDir + 'assets/js/',
                        filter: 'isFile'
                    }
                ],
            }
        },

        replace: {
            build: {
                options: {
                    patterns: [
                        {
                            match: 'buildAppCss',
                            replacement: 'build/app.min.css'
                        },
                        {
                            match: 'buildVendorCss',
                            replacement: 'build/vendor.min.css'
                        },
                        {
                            match: 'buildVendorJs',
                            replacement: 'build/vendor.js'
                        },
                        {
                            match: 'buildAppJs',
                            replacement: 'build/app.js'
                        }
                    ]
                },
                files: [
                    {expand: true, flatten: true, src: ['index.html'], dest: 'app/'}
                ]
            },
            dist: {
                options: {
                    patterns: [
                        {
                            match: 'buildAppCss',
                            replacement: 'assets/css/app.min.css'
                        },
                        {
                            match: 'buildVendorCss',
                            replacement: 'assets/css/vendor.min.css'
                        },
                        {
                            match: 'buildVendorJs',
                            replacement: 'assets/js/vendor.min.js'
                        },
                        {
                            match: 'buildAppJs',
                            replacement: 'assets/js/app.min.js'
                        }
                    ]
                },
                files: [
                    {expand: true, flatten: true, src: ['index.html'], dest: distDir}
                ]
            }
        },


        jscs: {
            options: {
                config: '.jscsrc',
                verbose: true,
                preset: 'airbnb'
            },
            all: {
                src: ['Gruntfile.js', coreDir + '**/*.js', mainDir + '**/*.js', layoutDir + '**/*.js']
            }
        },

        // configure watch to auto update ========================================================
        watch: {
            js: {
                files: ['index.html', appDir + 'app.module.js', mainDir + '*.html', coreDir + '**/*.js', mainDir + '**/*.js', layoutDir + '**/*.js'],
                tasks: ['jshint', 'concat:build', 'uglify:build', 'copy:build','replace:build'],
                options: {
                    livereload: true,
                }
            },
            css: {
                files: [assetsDir + 'css/*.css'],
                tasks: ['cssmin:build'],
                options: {
                    livereload: true,
                }
            }
        },

        connect: {
            server: {
                options: {
                    port: 9003,
                    base: 'app/'
                }
            }
        }

    });

    // LOAD GRUNT PLUGINS ========================================================
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-connect');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-jscs');
    grunt.loadNpmTasks('grunt-replace');

    // CREATE TASKS ==============================================================
    grunt.registerTask('build-vendor', ['clean:buildVendor', 'concat:buildVendor', 'uglify:buildVendor', 'cssmin:buildVendor']);
    grunt.registerTask('build-development', ['replace:build', 'jshint', 'concat:build', 'uglify:build', 'cssmin:build', 'copy:build']);
    grunt.registerTask('build-production', ['copy:folder', 'clean:distCss', 'clean:distJs', 'copy:buildFiles', 'replace:dist']);
    grunt.registerTask('default', ['build-development', 'connect', 'watch']);

};

