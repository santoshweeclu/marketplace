(function () {
    'use strict';

    angular.module('app', [
        'ngCookies',
        'ngMessages',
        'ngTouch',
        'ui.bootstrap',
        'ui.router',
        'ngMap',
        'ngTable',
        'toaster',
        'angular-loading-bar',
        'ngAnimate',
        'angularUtils.directives.dirPagination',
        'xeditable',
        'ngFileUpload',
        'angucomplete-alt'
    ]);

})();

(function () {
    'use strict';

    angular.module('app').config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider', 'paginationTemplateProvider', 'Constant'];

    function config($stateProvider, $urlRouterProvider, paginationTemplateProvider, Constant) {

        $urlRouterProvider.otherwise('/home');

        $stateProvider
            .state('home', {
                url: '/home',
                templateUrl: 'main/home/view/home.html',
                controller: 'homeController as vm',
                resolve: {
                    simpleObj: function () {
                        return {value: 'simple!'};
                    }
                }
            })
            .state('signup', {
                url: '/signup',
                templateUrl: 'layout/header/signup.html',
                controller: 'signupController as vm'
            })
            .state('signin', {
                url: '/signin',
                templateUrl: 'layout/header/signin.html',
                controller: 'signinController as vm'
            })
            .state('popup', {
                url: '/popup',
                templateUrl: 'popup/popup.html',
                controller: 'popupController as vm'
            })
            .state('product', {
                url: '/product',
                template: '<ui-view></ui-view>'
            })
            .state('product.cat', {
                url: '/:categoryId',
                templateUrl: 'main/product/list/product-list.html',
                controller: 'productListController as vm',
                resolve: {
                    ProductList: function (Constant, Common, $stateParams) {
                        var url = Constant.URL + Constant.CLIENT.SERVICES.GET_All_PRODUCT_BY_CATEGORY;
                        var params = {
                            category: $stateParams.categoryId
                        };

                        return Common.serviceCall('GET', url, params)
                            .then(function (response) {
                                return response.data;
                            });
                    }
                }
            })
            .state('product.cat.sub', {
                url: '/:subCategoryId',
                templateUrl: 'main/product/list/product-list.html',
                controller: 'productListController as vm',
                resolve: {
                    ProductList: function (Constant, Common, $stateParams) {
                        var url = Constant.URL + Constant.CLIENT.SERVICES.GET_All_PRODUCT_BY_CATEGORY;
                        var params = {
                            category: $stateParams.categoryId,
                            subcategory: $stateParams.subCategoryId
                        };

                        return Common.serviceCall('GET', url, params)
                            .then(function (response) {
                                return response.data;
                            });
                    }
                }
            })
           /* .state('product.grid', {
                url: '/grid',
                templateUrl: 'main/product/grid/product-grid.html',
                controller: 'productGridController as vm'
            })*/
            .state('product.cat.sub.detail', {
                url: '/:id',
                templateUrl: 'main/product/details/product-details.html',
                controller: 'productDetailsController as vm',
                resolve: {
                    ProductList: function (Constant, Common, $stateParams) {
                        var url = Constant.URL + Constant.CLIENT.SERVICES.GET_PRODUCT_BY_ID;
                        var params = {
                            yoId: $stateParams.id
                        };

                        return Common.serviceCall('GET', url, params)
                            .then(function (response) {
                                return response.data;
                            });
                    }
                }
            });

        //$locationProvider.html5Mode(true); //For Remove #


        paginationTemplateProvider.setPath(Constant.PAGINATION_TEMP);
    }

})();

(function () {
    'use strict';

    angular.module('app').constant('Constant', {
        URL:'http://10.0.0.19:8080/YoPillz',
        LOADER:true,
        IMAGE_PATH: 'assets/img/',
        JSON: 'assets/json/',
        PAGINATION_TEMP: 'layout/pagination/dirPagination.tpl.html',
        LOGIN:{
            VALIDATIONS:{},
            SERVICES:{
                AUTH:'/authentication',
                VALID_AUTH:'/validateauthtoken',
                GET_USER_DETAIL:'/userdetail',
                LOGOUT:'/logout'
            }
        },
        CLIENT:{
            VALIDATIONS:{},
            SERVICES:{
                GET_All_PRODUCT_BY_CATEGORY:'/getallproductbycategory',
                GET_PRODUCT_BY_ID:'/getproductbyid'
            }
        }
    });

})();

(function () {
    'use strict';

    angular.module('app').run(run);

    run.$inject = ['$rootScope', '$state', 'Authentication'];

    function run($rootScope, $state, Authentication) {

        $rootScope.$on('$stateChangeStart',
            function(event, toState, toParams, fromState, fromParams, options){
                var stateName = toState.name || '';
                var authTokan = Authentication.getAuthTokan();

                if (stateName === 'home'){
                    if (authTokan !== ''){
                        event.preventDefault();
                        $state.go('dashboard');
                    }
                }else{
                    if (authTokan === ''){
                        event.preventDefault();
                        $state.go('login');
                    }
                }

                if (toState.redirectTo) {
                    event.preventDefault();
                    $state.go(toState.redirectTo, toParams, {location: 'replace'});
                }
            });

        $rootScope.$on('$stateChangeSuccess',
            function (event, toState, toParams, fromState, fromParams) {
            });

    }

})();

(function () {
    'use strict';
    angular.module('app').directive('pieChart', function () {
        var loader = {
            template: '<div class="pie-chart" data-percent="{{percent}}"><div ng-transclude></div></div>',
            replace: true,
            transclude: true,
            link: linkFunction,
            scope:{
                percent:'@'
            }
        };

        return loader;
    });

    function linkFunction(scope, elem, attr) {
        var easyPieChartDefaults = {
            animate: 2000,
            scaleColor: false,
            lineWidth: 6,
            lineCap: 'square',
            size: 90,
            trackColor: '#e5e5e5'
        };
        $(elem).easyPieChart($.extend({}, easyPieChartDefaults, {
            barColor: PixelAdmin.settings.consts.COLORS[1]
        }));
    }
})();


(function () {
    'use strict';
    angular.module('app').directive('meanMenu', function () {
        var directive = {
            link: linkFunction
        };

        return directive;
    });

    function linkFunction(scope, elem, attr) {
        $(elem).meanmenu({
            meanScreenWidth: "991",
            meanMenuContainer: attr.menuClass,
        });
    }
})();


(function () {
    'use strict';
    angular.module('app').directive('lineChart', function () {
        var loader = {
            template: '<div class="graph" style="height: 230px;"></div>',
            replace: true,
            link: linkFunction
        };

        return loader;
    });

    function linkFunction(scope, elem, attr) {
        new Morris.Line({
            element: elem,
            data: [
                {day: '2014-03-10', v: 20},
                {day: '2014-03-11', v: 10},
                {day: '2014-03-12', v: 15},
                {day: '2014-03-13', v: 12},
                {day: '2014-03-14', v: 5},
                {day: '2014-03-15', v: 5},
                {day: '2014-03-16', v: 20}
            ],
            xkey: 'day',
            ykeys: ['v'],
            labels: ['Value'],
            lineColors: ['#fff'],
            lineWidth: 2,
            pointSize: 4,
            gridLineColor: 'rgba(255,255,255,.5)',
            resize: true,
            gridTextColor: '#fff',
            xLabels: 'day',
            xLabelFormat: function (d) {
                return ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'][d.getMonth()] + ' ' + d.getDate();
            },
        });
    }
})();


(function () {
    'use strict';
    angular.module('app').directive('nivoSlider', function () {
        var directive = {
            link: linkFunction
        };

        return directive;
    });

    function linkFunction(scope, elem, attr) {
        $(elem).nivoSlider({
            effect: 'random',
            slices: 15,
            boxCols: 12,
            boxRows: 4,
            animSpeed: 600,
            pauseTime: 5000,
            startSlide: 0,
            controlNav: false,
            controlNavThumbs: false,
            pauseOnHover: false,
            manualAdvance: false,
            prevText: '<i class="fa fa-chevron-left nivo-prev-icon"></i>',
            nextText: '<i class="fa fa-chevron-right nivo-next-icon"></i>'

        });
    }
})();


(function () {
    'use strict';
    angular.module('app').directive('owlCarousel', function () {
        var directive = {
            link: linkFunction
        };

        return directive;
    });

    function linkFunction(scope, elem, attr) {
        $(elem).owlCarousel({
            autoPlay: false,
            slideSpeed:2000,
            pagination:false,
            navigation:true,
            items : attr.items || 6,
            /* transitionStyle : "fade", */    /* [This code for animation ] */
            navigationText:["<i class='fa fa-angle-right'></i>","<i class='fa fa-angle-left'></i>"],
            itemsDesktop : [1199,3],
            itemsDesktopSmall : [980,3],
            itemsTablet: [768,2],
            itemsMobile : [479,1],
        });
    }
})();


(function () {
    'use strict';
    angular.module('app').directive('scrollUp', function () {
        var directive = {
            link: linkFunction
        };

        return directive;
    });

    function linkFunction(scope, elem, attr) {
        $(elem).scrollUp({
            scrollText: '<i class="fa fa-angle-up"></i>',
            easingType: 'linear',
            scrollSpeed: 900,
            animation: 'fade'
        });
    }
})();

(function () {
    'use strict';
    angular.module('app').directive('simpleLens', function () {
        var directive = {
            link: linkFunction
        };

        return directive;
    });

    function linkFunction(scope, elem, attr) {
        $(elem).simpleLens({
            loading_image: 'assets/img/loading.gif'
        });
    }
})();


(function () {
    'use strict';
    angular.module('app').directive('productCount', function () {
        var directive = {
            link: linkFunction
        };

        return directive;
    });

    function linkFunction(scope, elem, attr) {
        $(elem).prepend('<div class="dec qtybutton">-</div>');
        $(elem).append('<div class="inc qtybutton">+</div>');
        $(elem).find('.qtybutton').on('click', function() {
            var $button = $(this);
            var newVal = 0;
            var oldValue = $button.parent().find('input').val();
            if ($button.text() == '+') {
                newVal = parseFloat(oldValue) + 1;
            } else {
                // Don't allow decrementing below zero
                if (oldValue > 0) {
                    newVal = parseFloat(oldValue) - 1;
                } else {
                    newVal = 0;
                }
            }
            $button.parent().find('input').val(newVal);
        });
    }
})();


(function () {
    'use strict';
    angular.module('app').directive('skrollr', function () {
        var directive = {
            link: linkFunction
        };

        return directive;
    });

    function linkFunction(scope, elem, attr) {
        skrollr.init({forceHeight: false}).refresh();
    }
})();

(function () {
    'use strict';
    angular.module('app').directive('sparkLineChart', function () {
        var loader = {
            template: '<div class="stats-sparklines" style="width: 100%"></div>',
            replace: true,
            link: linkFunction
        };

        return loader;
    });

    function linkFunction(scope, elem, attr) {
        $(elem).pixelSparkline([275,490,397,487,339,403,402,312,300], {
            type: 'line',
            width: '100%',
            height: '45px',
            fillColor: '',
            lineColor: '#fff',
            lineWidth: 2,
            spotColor: '#ffffff',
            minSpotColor: '#ffffff',
            maxSpotColor: '#ffffff',
            highlightSpotColor: '#ffffff',
            highlightLineColor: '#ffffff',
            spotRadius: 4
        });
    }
})();


(function () {
    'use strict';
    angular.module('app').directive('stickHeader', function () {
        var directive = {
            link: linkFunction
        };

        return directive;
    });

    function linkFunction(scope, elem, attr) {
        var offset = $(attr.stickPoint).offset().top;
        //.header-mid-area .logo-area img
        $(window).scroll(function (e) {
            if ($(window).width() > 768) {
                $(elem).find('.header-top').addClass('header-top-stick');
                $(elem).find('.header-mid-area').addClass('header-mid-area-stick');
                $(elem).find('.header-bottom-area').addClass('header-bottom-area-stick');
                if ($(window).scrollTop() > offset) {
                    $(elem).find('.header-mid-area').addClass('after-scroll-point');
                } else {
                    $(elem).find('.header-mid-area').removeClass('after-scroll-point');
                }
            } else {
                $(elem).find('.header-top').removeClass('header-top-stick');
                $(elem).find('.header-mid-area').removeClass('header-mid-area-stick ');
                $(elem).find('.header-bottom-area').removeClass('header-bottom-area-stick');
            }

        });
    }
})();



(function () {
    'use strict';
    angular.module('app').factory('Common', common);

    common.$inject = ['$http'];

    function common($http) {
        return {
            data: {},
            serviceCall: serviceCall
        };

        function serviceCall(method, url, params, headers, data) {
            return $http({
                method: method,
                url: url,
                params: params || '',
                headers: headers || '',
                data: data || ''
            }).then(successFunction).catch(errorFunction);

            // SUCCESS
            function successFunction(response) {
                return response.data;
            }

            // ERROR
            function errorFunction(response) {
                // Attach the data
                return response;
            }
        }
    }
})();

(function () {
    'use strict';
    angular.module('app').factory('Authentication', authentication);

    authentication.$inject = ['$http', '$cookies'];

    function authentication($http, $cookies) {
        return {
            getAuthTokan: getAuthTokan,
            removeAuthTokan: removeAuthTokan,
            addAuthTokan: addAuthTokan
        };

        function getAuthTokan() {
            return $cookies.get('authentication') || '';
        }

        function removeAuthTokan() {
            $cookies.remove('authentication');
        }

        function addAuthTokan(data) {
            $cookies.put('authentication', data);
        }

        function verifyAuthTokan(verificationToken) {

            return $http.get(Constant.URL + Constant.LOGIN.SERVICES.GET_USER_DETAIL, {
                    header: {
                        authId: verificationToken
                    }
                })
                .then(success)
                .catch(error);

            function success(response) {
                return response.data;
            }

            function error(error) {
                console.log(error.data);
            }
        }
    }
})();

(function () {
    'use strict';
  angular.module('app').controller('homeController', homeController);

    homeController.$inject = ['$scope','$state', 'simpleObj', 'toaster', 'Constant', '$timeout' ];

    function homeController( $scope,$state, simpleObj, toaster, Constant, $timeout) {
        /*jshint validthis: true */

        var vm = this;
        vm.state = $state;
        vm.imagePath = Constant.IMAGE_PATH;
        vm.heading = simpleObj.value;
        vm.click = clickFunction;
        //$scope.places= 'null';
        function clickFunction() {
            toaster.pop('success', 'title', 'text');
        }


        $scope.selected = undefined;
        //lookup values
        $scope.countries = [
            {name: 'Madhapur'},
            {name: 'Gachibowli' },
            {name: 'kukatpally'},
            {name: 'miyapur'},
            {name: 'panjagutta'},
            {name: 'kondapur'},
            {name: 'begumpet'},
            {name: 'Malkajgiri'},
            {name: 'Mehadipatnam'},
            {name: 'Hi-tech city'},
        ];

/*---local storage the data----*/

       /*$scope.localStorageDemo = localStorageService.get('localStorageDemo');

        $scope.$watch('localStorageDemo', function(value){
            localStorageService.set('localStorageDemo',value);
            $scope.localStorageDemoValue = localStorageService.get('localStorageDemo');
        });

        $scope.storageType = 'Local storage';

console.log($scope.storageType,'local storage data');
       $scope.$watch(function(){
            return localStorageService.get('localStorageDemo');
        }, function(value){
            $scope.localStorageDemo = value;
        });

        $scope.clearAll = localStorageService.clearAll;*/
    }
})();

(function () {
    'use strict';
    angular.module('app').controller('loginController', loginController);

    loginController.$inject = ['$scope', '$state', 'Common', 'Authentication', 'toaster', 'Constant'];

    function loginController($scope, $state, Common, Authentication, toaster, Constant) {
        /*jshint validthis: true */
        var vm = this;

        vm.login = {};
        vm.state = $state;

        vm.loginFunction = loginFunction;

        //Authentication.removeAuthTokan();

        function loginFunction() {
            var url = Constant.URL + Constant.LOGIN.SERVICES.AUTH;

            Common.serviceCall('POST', url, '', '', vm.login)
                .then(function (response) {
                    if (response.code === 0) {
                        Authentication.addAuthTokan(response.data.sToken);
                        $state.go('dashboard');
                    } else {
                        toaster.pop('danger', 'Alert', response.message);
                    }

                });
        }
    }

})();

    (function () {
    'use strict';
    angular.module('app').controller('productDetailsController', productDetailsController);

    productDetailsController.$inject = ['$state', 'toaster', 'Constant', 'Common'];

    function productDetailsController($state, toaster, Constant, Common) {
        /*jshint validthis: true */
        var vm = this;
        vm.productDetail = false;
        vm.productSelected = {};
        vm.productDetailFunction = productDetailFunction;
        init();
        function init() {
            Common.serviceCall('GET', Constant.JSON_PATH + 'product-detail.json')
                .then(function (data) {
                    vm.productSelected = data;
                });
        }
        function productDetailFunction(){

        }
    }
})();

(function () {
    'use strict';
    angular.module('app').controller('productGridController', productGridController);

    productGridController.$inject = ['$state', 'toaster', 'Constant', 'Common','ProductList'];

    function productGridController($state, toaster, Constant, Common, ProductList) {
        /*jshint validthis: true */
        var vm = this;
        vm.state = $state;
        vm.productList = ProductList;
        vm.currentPage = 1;
        vm.pageLower = 1;
        vm.pageUpper = 10;
        vm.productDetailFunction = productDetailFunction;
        vm.pageChangeHandler = pageChangeHandler;

        init();

        function init() {

        }

        function clickFunction() {
            toaster.pop('success', 'title', 'text');
        }

        function productDetailFunction() {
        }

        function pageChangeHandler(newPage, oldPage) {
            console.log('going to page ' , newPage, oldPage);
        }
    }
})();

(function () {
    'use strict';
    angular.module('app').controller('productListController', productListController);

    productListController.$inject = ['$state', 'toaster', 'Constant', 'Common','ProductList'];

    function productListController($state, toaster, Constant, Common, ProductList) {
        /*jshint validthis: true */
        var vm = this;
        vm.state = $state;
        vm.productList = ProductList || [];
        vm.currentPage = 1;
        vm.pageLower = 1;
        vm.pageUpper = 10;
        vm.productDetailFunction = productDetailFunction;
        vm.pageChangeHandler = pageChangeHandler;

        init();

        function init() {
            console.log(ProductList);
            toaster.pop('success', 'title', 'text');
        }

        function clickFunction() {
            toaster.pop('success', 'title', 'text');
        }

        function productDetailFunction() {
        }

        function pageChangeHandler(newPage, oldPage) {
            console.log('going to page ' , newPage, oldPage);
        }

        //$('#myModal').modal('show');
    }
})();

(function () {
    'use strict';
    angular.module('app').controller('productController', productController);

    productController.$inject = ['$state', 'toaster', 'Constant', 'Common','$http'];

    function productController($state, toaster, Constant, Common, $http) {
        /*jshint validthis: true */
        var vm = this;
        vm.state = $state;

        vm.productDetailFunction = productDetailFunction;

        init();

        function init() {
            $state.go('product.list');
            /*Common.serviceCall('GET',Constant.JSON_PATH + 'product-list.json')
                .then(successFunction)
                .catch(errorFunction);

            function successFunction(data) {
                vm.productList = data;
            }

            function errorFunction() {
                console.log('Error with JSON file');
            }*/
        }

        function clickFunction() {
            toaster.pop('success', 'title', 'text');
        }

        function productDetailFunction() {
        }
    }
})();

(function () {
    'use strict';
    angular.module('app').component('breadCrumb', {
        templateUrl: 'layout/footer/footer.html',
        controllerAs: 'vm',
        controller: breadCrumbController
    });

    breadCrumbController.$inject = ['Constant'];

    function breadCrumbController(Constant) {
        /*jshint validthis: true */
        var vm = this;
    }
})();

(function () {
    'use strict';
    angular.module('app').component('appFooter', {
        templateUrl: 'layout/footer/footer.html',
        controllerAs: 'vm',
        controller: footerController
    });

    footerController.$inject = ['Constant'];

    function footerController(Constant) {
        /*jshint validthis: true */
        var vm = this;
        vm.imagePath = Constant.IMAGE_PATH;
    }
})();

(function () {
    'use strict';
    angular.module('app').component('appHeader', {
        templateUrl: 'layout/header/header.html',
        controllerAs: 'vm',
        controller: headerController,
        bindings: {
            state: '='
        }
    });

    headerController.$inject = ['$state', 'Authentication', 'toaster', 'Common', 'Constant'];

    function headerController($state, Authentication, toaster, Common, Constant) {
        /*jshint validthis: true */
        var vm = this;
        vm.imagePath = Constant.IMAGE_PATH;

        vm.logoutFunction = logoutFunction;

        vm.userInfo = {};
        vm.userInfo.firstName = 'Admin';

        vm.productCategory = [];

        var AuthTokan = Authentication.getAuthTokan();
        var url = '';
        var header = {
            authId: ''
        };

        getProductCategories();

        if (AuthTokan !== '') {
            init();
        }

        function init() {
            url = Constant.URL + Constant.LOGIN.SERVICES.GET_USER_DETAIL;
            header.authId = AuthTokan;

            getUserDetail(url, header);
            autoCompleteFunction();

        }

        function getUserDetail(url, header) {
            Common.serviceCall('GET', url, '', header, '')
                .then(function (response) {
                    switch (response.code) {
                        case 0:
                            vm.userInfo = response.data;
                            break;
                        default:
                            Authentication.removeAuthTokan();
                            $state.go('login');
                            console.log(response.message);
                    }
                })
                .catch(function (error) {
                    console.log('Get User Detail Service Error', error);
                });
        }

        function getProductCategories() {
            Common.serviceCall('GET', Constant.JSON + 'products-category.json')
                .then(function (response) {
                    vm.productCategory = response;
                });
        }

        function autoCompleteFunction() {
            Common.serviceCall('GET', 'http://localhost:9001/assets/json/counties.json')
                .then(function (response) {
                    vm.countries = response;
                })
                .catch(function (error) {
                    console.log('autoComplete Service Error', error);
                });
        }

        function logoutFunction() {
            url = Constant.URL + Constant.LOGIN.SERVICES.LOGOUT;
            header.authId = AuthTokan;

            Common.serviceCall('POST', url, '', header, '')
                .then(function (response) {
                    Authentication.removeAuthTokan();
                    toaster.pop('success', 'Good Bye...', vm.userInfo.firstName);
                    $state.go('login');
                })
                .catch(function (error) {
                    console.log('Logout Service Error', error);
                });
        }
    }
})();

(function () {
    'use strict';
    angular.module('app').controller('signinController', signinController);

    signinController.$inject = ['$scope', '$state', 'Common', 'Authentication', 'toaster', 'Constant'];

    function signinController($scope, $state, Common, Authentication, toaster, Constant) {
        /*jshint validthis: true */

        var vm = this;

        vm.login = {};
        vm.state = $state;

        vm.loginFunction = loginFunction;

        //Authentication.removeAuthTokan();

        function loginFunction() {

            var url = Constant.URL + Constant.LOGIN.SERVICES.AUTH;

            Common.serviceCall('POST', url, '', '', vm.login)
                .then(function (response) {
                    if (response.code === 0) {
                        Authentication.addAuthTokan(response.data.sToken);
                        $state.go('dashboard');
                    } else {
                        toaster.pop('danger', 'Alert', response.message);
                    }

                });
        }
    }

})();

(function () {
    'use strict';
    angular.module('app').controller('signupController', signupController);

    signupController.$inject = ['$scope', '$state', 'Common', 'Authentication', 'toaster', 'Constant'];

    function signupController($scope, $state, Common, Authentication, toaster, Constant) {
        /*jshint validthis: true */
        var vm = this;

        vm.login = {};
        vm.state = $state;

        vm.loginFunction = loginFunction;

        //Authentication.removeAuthTokan();

        function loginFunction() {
            var url = Constant.URL + Constant.LOGIN.SERVICES.AUTH;

            Common.serviceCall('POST', url, '', '', vm.login)
                .then(function (response) {
                    if (response.code === 0) {
                        Authentication.addAuthTokan(response.data.sToken);
                        $state.go('dashboard');
                    } else {
                        toaster.pop('danger', 'Alert', response.message);
                    }

                });
        }
    }

})();

(function () {
    'use strict';
    angular.module('app').directive('appLoader', function (Constant) {
        var directive = {
            templateUrl: 'layout/loader/loader.html',
            link: function (scope, elem, attr) {
                scope.widthStyle = attr.widthStyle;
                scope.show = Constant.LOADER;
            }
        };
        return directive;
    });
})();


(function () {
    'use strict';
    angular.module('app').component('appSidebar', {
        templateUrl: 'layout/sidebar/sidebar.html',
        controller: sidebarController,
        controllerAs: 'vm',
        bindings: {
            state: '='
        }
    });

    sidebarController.$inject = ['$state'];

    function sidebarController($state) {
        /*jshint validthis: true */
        var vm = this;
    }
})();
